#include "Piece.h"

bool Piece::checkMove(Piece* checkPiece, int i, sf::Vector2f oldPos, sf::Vector2f newPos, Color type)
{
    if (i < 2)
    {
        return ((Rook*)checkPiece)->checkMove(oldPos, newPos);
    }
    else if (i >= 2 && i < 4)
    {
        return ((Knight*)checkPiece)->checkMove(oldPos, newPos);
    }
    else if (i >= 4 && i < 6)
    {
        return ((Bishop*)checkPiece)->checkMove(oldPos, newPos);
    }
    else if (i == 6)
    {
        return ((Queen*)checkPiece)->checkMove(oldPos, newPos);
    }
    else if (i == 7)
    {
        return ((King*)checkPiece)->checkMove(oldPos, newPos);
    }
    else
    {
        return ((Pawn*)checkPiece)->checkMove(oldPos, newPos, type);
    }
}

void Rook::loadPiece(Color type)
{
    sf::Texture newTexture;
    switch (type)
    {
    case black:
        newTexture.loadFromFile("chess_pieces/b_rook.png");
        setTexture(newTexture);
        break;
    case white:
        newTexture.loadFromFile("chess_pieces/w_rook.png");
        setTexture(newTexture);
        break;
    }
}

bool Rook::checkMove(sf::Vector2f oldPos, sf::Vector2f newPos)
{
    if (oldPos.x != newPos.x)
    {
        if (oldPos.y != newPos.y)
        {
            return false;
        }
        return true;
    }
    else if (oldPos.y != newPos.y)
    {
        if (oldPos.x != newPos.x)
        {
            return false;
        }
        return true;
    }
}



void Knight::loadPiece(Color type)
{
    sf::Texture newTexture;
    switch (type)
    {
    case black:
        newTexture.loadFromFile("chess_pieces/b_knight.png");
        setTexture(newTexture);
        break;
    case white:
        newTexture.loadFromFile("chess_pieces/w_knight.png");
        setTexture(newTexture);
        break;
    }
}

bool Knight::checkMove(sf::Vector2f oldPos, sf::Vector2f newPos)
{
    if (oldPos.x == newPos.x + 150)
    {
        if (oldPos.y == newPos.y + 75)
        {
            return true;
        }
        else if (oldPos.y == newPos.y - 75)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else if (oldPos.x == newPos.x - 150)
    {
        if (oldPos.y == newPos.y + 75)
        {
            return true;
        }
        else if (oldPos.y == newPos.y - 75)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else if (oldPos.y == newPos.y + 150)
    {
        if (oldPos.x == newPos.x + 75)
        {
            return true;
        }
        else if (oldPos.x == newPos.x - 75)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else if (oldPos.y == newPos.y - 150)
    {
        if (oldPos.x == newPos.x + 75)
        {
            return true;
        }
        else if (oldPos.x == newPos.x - 75)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

void Bishop::loadPiece(Color type)
{
    sf::Texture newTexture;
    switch (type)
    {
    case black:
        newTexture.loadFromFile("chess_pieces/b_bishop.png");
        setTexture(newTexture);
        break;
    case white:
        newTexture.loadFromFile("chess_pieces/w_bishop.png");
        setTexture(newTexture);
        break;
    }
}

bool Bishop::checkMove(sf::Vector2f oldPos, sf::Vector2f newPos)
{
    int tempX = 0;
    int tempY = 0;
    if (oldPos.x < newPos.x)
    {
        tempX = newPos.x - oldPos.x;
        if (oldPos.y < newPos.y)
        {
            tempY = newPos.y - oldPos.y;
        }
        else if (oldPos.y > newPos.y)
        {
            tempY = oldPos.y - newPos.y;
        }
        if (tempX / 75 == tempY / 75)
        {
            return true;
        }
    }
    else if (oldPos.x > newPos.x)
    {
        tempX = oldPos.x - newPos.x;
        if (oldPos.y < newPos.y)
        {
            tempY = newPos.y - oldPos.y;
        }
        else if (oldPos.y > newPos.y)
        {
            tempY = oldPos.y - newPos.y;
        }
        if (tempX / 75 == tempY / 75)
        {
            return true;
        }
    }
    return false;

}

void Queen::loadPiece(Color type)
{
    sf::Texture newTexture;
    switch (type)
    {
    case black:
        newTexture.loadFromFile("chess_pieces/b_queen.png");
        setTexture(newTexture);
        break;
    case white:
        newTexture.loadFromFile("chess_pieces/w_queen.png");
        setTexture(newTexture);
        break;
    }
}

bool Queen::checkMove(sf::Vector2f oldPos, sf::Vector2f newPos)
{
    int tempX = 0;
    int tempY = 0;
    if (oldPos.x < newPos.x)
    {
        if (oldPos.y == newPos.y)
        {
            return true;
        }
        tempX = newPos.x - oldPos.x;
        if (oldPos.y < newPos.y)
        {
            tempY = newPos.y - oldPos.y;
        }
        else if (oldPos.y > newPos.y)
        {
            tempY = oldPos.y - newPos.y;
        }
        if (tempX / 75 == tempY / 75)
        {
            return true;
        }
    }
    else if (oldPos.x > newPos.x)
    {
        if (oldPos.y == newPos.y)
        {
            return true;
        }
        tempX = oldPos.x - newPos.x;
        if (oldPos.y < newPos.y)
        {
            tempY = newPos.y - oldPos.y;
        }
        else if (oldPos.y > newPos.y)
        {
            tempY = oldPos.y - newPos.y;
        }
        if (tempX / 75 == tempY / 75)
        {
            return true;
        }
    }
    else if (oldPos.x == newPos.x)
    {
        if (oldPos.y != newPos.y)
        {
            return true;
        }
    }
    return false;
}

void King::loadPiece(Color type)
{
    sf::Texture newTexture;
    switch (type)
    {
    case black:
        newTexture.loadFromFile("chess_pieces/b_king.png");
        setTexture(newTexture);
        break;
    case white:
        newTexture.loadFromFile("chess_pieces/w_king.png");
        setTexture(newTexture);
        break;
    }
}

bool King::checkMove(sf::Vector2f oldPos, sf::Vector2f newPos)
{
    if (oldPos.x == newPos.x + 75 || oldPos.x == newPos.x - 75)
    {
        if (oldPos.y == newPos.y + 75 || oldPos.y == newPos.y - 75 || oldPos.y == newPos.y)
        {
            return true;
        }
        return false;
    }
    if (oldPos.y == newPos.y + 75 || oldPos.y == newPos.y - 75)
    {
        if (oldPos.x == newPos.x + 75 || oldPos.x == newPos.x - 75 || oldPos.x == newPos.x)
        {
            return true;
        }
        return false;
    }
    else
    {
        return false;
    }
}

void Pawn::loadPiece(Color type)
{
    sf::Texture newTexture;
    switch (type)
    {
    case black:
        newTexture.loadFromFile("chess_pieces/b_pawn.png");
        setTexture(newTexture);
        break;
    case white:
        newTexture.loadFromFile("chess_pieces/w_pawn.png");
        setTexture(newTexture);
        break;
    }
}

bool Pawn::checkMove(sf::Vector2f oldPos, sf::Vector2f newPos, Color type)
{

    switch (type)
    {
    case white:
        if (oldPos.y == 475)
        {
            if (newPos.y != oldPos.y - 150 && newPos.y != oldPos.y - 75)
            {
                return false;
            }
            if (newPos.x != oldPos.x)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            if (newPos.y != oldPos.y - 75)
            {
                return false;
            }
            if (newPos.x != oldPos.x)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    case black:
        if (oldPos.y == 100)
        {
            if (newPos.y != oldPos.y + 150 && newPos.y != oldPos.y + 75)
            {
                return false;
            }
            if (newPos.x != oldPos.x)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            if (newPos.y != oldPos.y + 75)
            {
                return false;
            }
            if (newPos.x != oldPos.x)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}