#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>
#include <windows.h>
#include "Board.h"

using std::cout;
using std::endl;
using std::cin;
using std::string;

enum Color
{
    white, black
};


class Piece
{
public:
    Piece() {}
    ~Piece() {}


    virtual void loadPiece(Color type)
    {

    }
    virtual void displayPiece(sf::RenderWindow& window)
    {

    }

    sf::Texture getTexture()
    {
        return pieceTexture;
    }
    void setTexture(sf::Texture newTexture)
    {
        pieceTexture = newTexture;
        setSprite(0, 0);
    }

    void movePiece(sf::Vector2f movement)
    {
        pieceSprite.move(movement);
    }

    sf::Sprite getSprite()
    {
        return pieceSprite;
    }
    void setSprite(int x, int y)
    {
        pieceSprite.setTexture(pieceTexture);
        pieceSprite.move(x, y);
    }
    virtual void movePiece(int x, int y)
    {
        pieceSprite.setPosition(x, y);
    }
    virtual bool checkMove(Piece* checkPiece, int i, sf::Vector2f oldPos, sf::Vector2f newPos, Color type);

protected:
    sf::Texture pieceTexture;
    sf::Sprite pieceSprite;
};

class Pawn : public Piece
{

public:
    Pawn() {}
    ~Pawn() {}

    void loadPiece(Color type);
    bool checkMove(sf::Vector2f oldPos, sf::Vector2f newPos, Color type);
};

class Rook : public Piece
{

public:
    Rook() {}
    ~Rook() {}

    void loadPiece(Color type);
    bool checkMove(sf::Vector2f oldPos, sf::Vector2f newPos);
};

class Knight : public Piece
{
public:
    Knight() {}
    ~Knight() {}

    void loadPiece(Color type);
    bool checkMove(sf::Vector2f oldPos, sf::Vector2f newPos);
};

class Bishop : public Piece
{

public:
    Bishop() {}
    ~Bishop() {}

    void loadPiece(Color type);
    bool checkMove(sf::Vector2f oldPos, sf::Vector2f newPos);
};

class Queen : public Piece
{

public:
    Queen() {}
    ~Queen() {}

    void loadPiece(Color type);
    bool checkMove(sf::Vector2f oldPos, sf::Vector2f newPos);
};

class King : public Piece
{

public:
    King() {}
    ~King() {}

    void loadPiece(Color type);
    bool checkMove(sf::Vector2f oldPos, sf::Vector2f newPos);
};

Piece* loadPieces(Piece* newPiece, Color type, int i);
int determinePos(int x);
void printPosition(int i, sf::Vector2f position, bool taken);
string findNotation(sf::Vector2f position);