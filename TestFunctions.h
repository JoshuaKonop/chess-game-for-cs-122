#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>
#include <windows.h>

#include "Piece.h"

//All 5 test functions
bool runTestBoard(); //Sees if the board can be created
bool runTestPiece(); //Sees if the pieces can be created
bool runTestMove(); //Sees if the pieces can move
bool runTestCapture(); //Sees if the pieces can be captured
bool runTestTurn(); //Simulates turns, sees if turns work