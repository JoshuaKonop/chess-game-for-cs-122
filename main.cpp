#include "TestFunctions.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;

int main()
{

    int user_input = 0, user_input2 = 0;
    while (user_input != 3)
    {
        system("CLS");
        cout << "1. Play game." << endl;
        cout << "2. Display Rules." << endl;
        cout << "3. Exit." << endl;
        cout << "4. Run test diagnostics" << endl;
        cin >> user_input;
        if (user_input == 1)
        {
            system("cls");
            sf::RenderWindow window(sf::VideoMode(640, 640), "Chess");
            Board boardObj;
            Piece* playerOne[16];//white pieces
            Piece* playerTwo[16];//black pieces
            for (int i = 0; i < 16; i++)
            {
                playerOne[i] = loadPieces(playerOne[i], white, i);
                playerTwo[i] = loadPieces(playerTwo[i], black, i);
            }
            playerOne[0]->setSprite(25, 550);
            playerOne[1]->setSprite(550, 550);
            playerOne[2]->setSprite(100, 550);
            playerOne[3]->setSprite(475, 550);
            playerOne[4]->setSprite(175, 550);
            playerOne[5]->setSprite(400, 550);
            playerOne[6]->setSprite(250, 550);
            playerOne[7]->setSprite(325, 550);
            for (int i = 0; i < 8; i++)
            {
                playerOne[i + 8]->setSprite(25 + i * 75, 475);
            }
            playerTwo[0]->setSprite(25, 25);
            playerTwo[1]->setSprite(550, 25);
            playerTwo[2]->setSprite(100, 25);
            playerTwo[3]->setSprite(475, 25);
            playerTwo[4]->setSprite(175, 25);
            playerTwo[5]->setSprite(400, 25);
            playerTwo[6]->setSprite(250, 25);
            playerTwo[7]->setSprite(325, 25);
            for (int i = 0; i < 8; i++)
            {
                playerTwo[i + 8]->setSprite(25 + i * 75, 100);
            }
            float xPos = 0, yPos = 0;

            bool whiteSelected = false;
            bool blackSelected = false;
            bool whiteTurn = true;
            bool taken = true;
            cout << "White to move... ";
            int pieceSelection = 0;
            sf::Vector2f position;
            while (window.isOpen())
            {
                sf::Vector2i cursor = sf::Mouse::getPosition(window); //detects the position of users cursor
                sf::Event event;
                while (window.pollEvent(event))
                {
                    if (event.type == sf::Event::Closed)
                    {
                        window.close();
                    }
                    if (event.type == sf::Event::MouseButtonPressed)
                    {
                        if (event.key.code == sf::Mouse::Left) //detects left click
                        {
                            for (int i = 0; i < 16; i++)//loops through both piece arrays
                            {
                                //The conditionals below detect if the position of the cursor is over a piece sprite.
                                //If so bool "whiteSelected" or "blackSelected" will be set to true until the left mouse
                                //is released. 
                                if (whiteTurn == true && playerOne[i] != nullptr && playerOne[i]->getSprite().getGlobalBounds().contains(cursor.x, cursor.y))
                                {
                                    whiteSelected = true;
                                    pieceSelection = i; //assigns pieceSelection the location of the piece in the array
                                    //xPos and yPos are set to the position of the cursor minus the position of the sprite.
                                    //This is used to prevent the piece from shifting when it is selected.
                                    xPos = cursor.x - playerOne[i]->getSprite().getPosition().x;
                                    yPos = cursor.y - playerOne[i]->getSprite().getPosition().y;
                                    position = playerOne[i]->getSprite().getPosition(); //records the position the piece started in when it is moved
                                }
                                if (whiteTurn == false && playerTwo[i] != nullptr && playerTwo[i]->getSprite().getGlobalBounds().contains(cursor.x, cursor.y))
                                {
                                    blackSelected = true;
                                    pieceSelection = i;
                                    xPos = cursor.x - playerTwo[i]->getSprite().getPosition().x;
                                    yPos = cursor.y - playerTwo[i]->getSprite().getPosition().y;
                                    position = playerTwo[i]->getSprite().getPosition();
                                }
                            }
                        }
                    }
                    if (event.type == sf::Event::MouseButtonReleased)
                    {
                        if (event.key.code == sf::Mouse::Left) //detects when left click is released
                        {
                            if (whiteSelected == true)//If you place a white piece this will run
                            {
                                whiteTurn = false;
                                playerOne[pieceSelection]->movePiece(determinePos(playerOne[pieceSelection]->getSprite().getPosition().x), determinePos(playerOne[pieceSelection]->getSprite().getPosition().y));
                                sf::Vector2f currentPos = playerOne[pieceSelection]->getSprite().getPosition();
                                if (playerOne[pieceSelection]->checkMove(playerOne[pieceSelection], pieceSelection, position, currentPos, white) == false)
                                {
                                    playerOne[pieceSelection]->movePiece(position.x, position.y);
                                    whiteTurn = true;
                                }
                                sf::Vector2f bPos;
                                position = playerOne[pieceSelection]->getSprite().getPosition();
                                for (int i = 0; i < 16; i++)
                                {
                                    if (playerTwo[i] != nullptr) //this filters out taken pieces
                                    {
                                        bPos = playerTwo[i]->getSprite().getPosition();
                                    }
                                    //The big conditonal below checks to see if the piece you drag and dropped is within 25 pixles of an opposing piece
                                    if (bPos.x < position.x + 25 && bPos.x > position.x - 25 && bPos.y < position.y + 25 && bPos.y > position.y - 25)
                                    {
                                        playerTwo[i] = nullptr;//sets the opposing piece to nullptr thus removing it form the screen.
                                        taken = true;
                                    }
                                }
                                if (whiteTurn == false)
                                {
                                    printPosition(pieceSelection, currentPos, taken);
                                    cout << "Black to move... ";
                                }
                            }
                            if (blackSelected == true) //identical to the white piece conditional above.
                            {
                                whiteTurn = true;
                                playerTwo[pieceSelection]->movePiece(determinePos(playerTwo[pieceSelection]->getSprite().getPosition().x), determinePos(playerTwo[pieceSelection]->getSprite().getPosition().y));
                                sf::Vector2f currentPos = playerTwo[pieceSelection]->getSprite().getPosition();
                                if (playerTwo[pieceSelection]->checkMove(playerTwo[pieceSelection], pieceSelection, position, currentPos, black) == false)
                                {
                                    playerTwo[pieceSelection]->movePiece(position.x, position.y);
                                    whiteTurn = false;
                                }

                                sf::Vector2f wPos;
                                position = playerTwo[pieceSelection]->getSprite().getPosition();
                                for (int i = 0; i < 16; i++)
                                {
                                    if (playerOne[i] != nullptr)
                                    {
                                        wPos = playerOne[i]->getSprite().getPosition();
                                    }
                                    if (wPos.x < position.x + 25 && wPos.x > position.x - 25 && wPos.y < position.y + 25 && wPos.y > position.y - 25)
                                    {
                                        playerOne[i] = nullptr;
                                        taken = true;
                                    }
                                }
                                if (whiteTurn == true)
                                {
                                    printPosition(pieceSelection, currentPos, taken);
                                    cout << "White to move... ";
                                }
                            }
                            whiteSelected = false;
                            blackSelected = false;
                            taken = false;
                        }
                    }
                }
                //The two conditionals below update the position of the piece you are currently moving
                if (whiteSelected == true)
                {
                    playerOne[pieceSelection]->movePiece(cursor.x - xPos, cursor.y - yPos);
                }
                if (blackSelected == true)
                {
                    playerTwo[pieceSelection]->movePiece(cursor.x - xPos, cursor.y - yPos);
                }

                window.clear();
                window.draw(boardObj.getSprite());
                for (int i = 0; i < 16; i++)
                {
                    //loops through each array drawing all pieces.
                    if (playerOne[i] != nullptr)
                    {
                        window.draw(playerOne[i]->getSprite());
                    }
                    if (playerTwo[i] != nullptr)
                    {
                        window.draw(playerTwo[i]->getSprite());
                    }
                }
                window.display();
            }
        }
        else if (user_input == 2)
        {
            user_input2 = 0;
            while (user_input2 != 9)
            {
                system("CLS");
                cout << "Press a number to see the corresponding rules for said number." << endl;
                cout << "1. Pawn." << endl;
                cout << "2. Rook." << endl;
                cout << "3. Knight." << endl;
                cout << "4. Bishop." << endl;
                cout << "5. Queen." << endl;
                cout << "6. King." << endl;
                cout << "7. Special Rules." << endl;
                cout << "8. How to win." << endl;
                cout << "9. Return to main menu." << endl;
                cin >> user_input2;
                if (user_input2 == 1)
                {
                    system("CLS");
                    cout << "Pawns are the only piece in chess that captures" << endl;
                    cout << "differently than it moves. The pawn moves foward" << endl;
                    cout << "one space at a time, except for the first move," << endl;
                    cout << "where it can move forward two spaces. However, it " << endl;
                    cout << "captures diagonally. It can only capture from one" << endl;
                    cout << "space away and it cannot capture from behind." << endl << endl;
                    system("pause");
                }
                else if (user_input2 == 2)
                {
                    system("CLS");
                    cout << "The rook moves vertically or horizontally as many" << endl;
                    cout << "spaces as it wants as long as it doesn't go through" << endl;
                    cout << "any of its own pieces." << endl << endl;
                    system("pause");
                }
                else if (user_input2 == 3)
                {
                    system("CLS");
                    cout << "Knights have a unique movement pattern. The knight" << endl;
                    cout << "moves two spaces in one direction then one more space" << endl;
                    cout << "at a 90 degree angle. Think of the shape of an 'L'." << endl;
                    cout << "The knight is also the only piece that can move over" << endl;
                    cout << "other pieces." << endl << endl;
                    system("pause");
                }
                else if (user_input2 == 4)
                {
                    system("CLS");
                    cout << "The bishop moves diagonally as many spaces as it wants" << endl;
                    cout << "as long as it doesn't go through any of its own pieces." << endl << endl;
                    system("pause");
                }
                else if (user_input2 == 5)
                {
                    system("CLS");
                    cout << "The queen can move as many spaces vertically, horizontally" << endl;
                    cout << "or diagonally as long as it doesn't go through any of its" << endl;
                    cout << "own pieces." << endl << endl;
                    system("pause");
                }
                else if (user_input2 == 6)
                {
                    system("CLS");
                    cout << "The king can move exaclty one space in any direction." << endl;
                    cout << "However, it is the most important piece in the game" << endl;
                    cout << "because if at any point you're in check you must find" << endl;
                    cout << "find a way to get out of check." << endl << endl;
                    system("pause");
                }
                else if (user_input2 == 7)
                {
                    system("CLS");
                    cout << "Promoting: If a pawn makes it to the back row of the" << endl;
                    cout << "opposite side of the board it can promote to any piece" << endl;
                    cout << "besides a king." << endl << endl;
                    cout << "En Passant: If a pawn moves out two squares on its first move" << endl;
                    cout << "and by doing so lands to the side of an opponent's pawn" << endl;
                    cout << "The opponents pawn has the option to take said pawn by moving" << endl;
                    cout << "Behind it. " << endl << endl;
                    cout << "Castling: The king and the rook can trade places so long" << endl;
                    cout << "as the player meets the four following criteria: " << endl;
                    cout << "1. It must be the king's first move." << endl;
                    cout << "2. It must be the rook's first move." << endl;
                    cout << "3. There cannot be any pieces between the king and the rook." << endl;
                    cout << "4. The king may not be in check or pass through check." << endl << endl;
                    system("pause");
                }
                else if (user_input2 == 8)
                {
                    system("CLS");
                    cout << "In order to win the game of chess you" << endl;
                    cout << "need to put your opponent in checkmate." << endl;
                    cout << "This happens when the king is put in" << endl;
                    cout << "check and has no way to get out of check." << endl;
                    cout << "The king is considered to be in 'check'" << endl;
                    cout << "when it is under attack." << endl << endl;
                    system("pause");
                }
                else if (user_input2 < 1 || user_input > 8)
                {
                    system("CLS");
                    cout << "Invalid input, try again." << endl << endl;
                    system("pause");
                }
            }
        }
        else if (user_input == 4) {
            system("cls");
            cout << "Running test diagnostics:" << endl;
            
            if (runTestBoard()) {
                if (runTestPiece()) {
                    if (runTestMove()) {
                        if (runTestCapture()) {
                            if (runTestTurn()) {
                                cout << "All tests passed!" << endl;
                            }
                        }
                    }
                }
            }
            else {
                cout << "Some test failed!" << endl;
            }
            Sleep(2000);
        }
        else if (user_input < 1 || user_input > 4)
        {
            system("CLS");
            cout << "Invalid input, try again." << endl;
            system("pause");
        }
    }
    cout << "Closing program..." << endl;
    Sleep(2000);
    return 0;
}