#include "TestFunctions.h"

bool runTestBoard() {
	//Will create a board. If the board is successfully created (window is able to open), then it will print out that the test was successful

	sf::RenderWindow testWindow(sf::VideoMode(640, 640), "Test");
	Board testBoard; //Create board
	testBoard.loadBoard();

	
	while (testWindow.isOpen()) { //If the window can successfully open, print out the board
		testWindow.clear();
		testWindow.draw(testBoard.getSprite());
		testWindow.display();

		cout << "Board generation test: Passed!" << endl; //Passed!
		Sleep(2000);
		testWindow.close();
		return true;
	}

	cout << "Board generation test: Failed!" << endl;
	return false;
}
bool runTestPiece() {
	//Will create 3 pieces on the board - A w_knight, b_pawn, and b_rook. If these are successfully created, then it will print out the test success
	sf::RenderWindow testWindow(sf::VideoMode(640, 640), "Test");
	Board testBoard;
	testBoard.loadBoard();

	Piece* testKnight = new Knight(); //Create and load pieces
	testKnight->loadPiece(white);
	Piece* testPawn = new Pawn();
	testPawn->loadPiece(black);
	Piece* testRook = new Rook();
	testRook->loadPiece(black);
	
	testKnight->setSprite(250, 175); //Place them on the board
	testPawn->setSprite(325, 325);
	testRook->setSprite(400, 100);

	while (testWindow.isOpen()) {
		testWindow.clear(); //Draw pieces
		testWindow.draw(testBoard.getSprite());
		testWindow.draw(testKnight->getSprite());
		testWindow.draw(testPawn->getSprite());
		testWindow.draw(testRook->getSprite());
		testWindow.display();

		cout << "Piece generation test: Passed!" << endl;
		Sleep(2000);
		testWindow.close();
		return true;
		
	}
	
	cout << "Piece generation test: Failed!" << endl;
	return false;
}
bool runTestMove() {
	//Will create a board and pieces, then move the pieces. If this is done successfully, then print it out.

	sf::RenderWindow testWindow(sf::VideoMode(640, 640), "Test");
	Board testBoard; //Create board
	testBoard.loadBoard();

	Piece* testKnight = new Knight(); //Create and load pieces
	testKnight->loadPiece(white);
	Piece* testPawn = new Pawn();
	testPawn->loadPiece(black);
	Piece* testRook = new Rook();
	testRook->loadPiece(black);

	testKnight->setSprite(250, 175); //Set pieces on the board
	testPawn->setSprite(325, 325);
	testRook->setSprite(400, 100);

	int counter = 0;
	while (testWindow.isOpen()) { //If window can properly open
		testWindow.clear();
		testWindow.draw(testBoard.getSprite()); //Draw sprites
		testWindow.draw(testKnight->getSprite());
		testWindow.draw(testPawn->getSprite());
		testWindow.draw(testRook->getSprite());
		testWindow.display();
		Sleep(2000);

		if (counter == 1) { //Second iteration closes the test
			testWindow.close();
			cout << "Piece movement test: Passed!" << endl;
			return true;

		}
		else { //This is the first iteration, it just moves the pieces
			testKnight->movePiece(sf::Vector2f(75, 150));
			testPawn->movePiece(sf::Vector2f(0, -75));
			testRook->movePiece(sf::Vector2f(-225, 0));
			counter++;
		}
	}

	cout << "Piece movement test: Failed!" << endl;
	return false;
}
bool runTestCapture() {
	//Does the same as the last 3, but only moves the knight onto the pawn. If the capturing ability functions, then it prints a success

	sf::RenderWindow testWindow(sf::VideoMode(640, 640), "Test");
	Board testBoard; //Create board
	testBoard.loadBoard();

	Piece* testKnight = new Knight(); //Create pieces
	testKnight->loadPiece(white);
	Piece* testPawn = new Pawn();
	testPawn->loadPiece(black);
	Piece* testRook = new Rook();
	testRook->loadPiece(black);

	testKnight->setSprite(250, 175); //Create sprites
	testPawn->setSprite(325, 325);
	testRook->setSprite(400, 100);

	int counter = 0;
	sf::Vector2f bPos;
	sf::Vector2f position = testKnight->getSprite().getPosition(); //Position variables
	while (testWindow.isOpen()) {
		testWindow.clear();
		testWindow.draw(testBoard.getSprite()); //Draw pieces
		testWindow.draw(testKnight->getSprite());
		if (testPawn != nullptr) { //If the pawn is not null
			testWindow.draw(testPawn->getSprite());
		}
		testWindow.draw(testRook->getSprite());
		testWindow.display();
		Sleep(2000);

		position = testKnight->getSprite().getPosition();
		if (testPawn != nullptr) //this filters out taken pieces
		{
			bPos = testPawn->getSprite().getPosition();
		}
		if (bPos.x < position.x + 25 && bPos.x > position.x - 25 && bPos.y < position.y + 25 && bPos.y > position.y - 25)
		{
			testPawn = nullptr;//sets the opposing piece to nullptr thus removing it form the screen
		}
		

		if (counter == 2) { //On last iteration, close the window
			testWindow.close();
			cout << "Piece capture test: Passed!" << endl;
			return true;

		}
		else {//First 2 iterations
			if (counter == 0) {
				testKnight->movePiece(sf::Vector2f(75, 150)); //On first iteration, move the knight onto the pawn
			}
			counter++;
		}
	}

	cout << "Piece capture test: Passed!" << endl;
	return false;
}
bool runTestTurn() {
	//Then, it checks who's turn it is. If it is blacks turn, then print out a success

	sf::RenderWindow testWindow(sf::VideoMode(640, 640), "Test");
	Board testBoard; //Create board
	testBoard.loadBoard();

	Piece* testKnight = new Knight(); //Create pieces
	testKnight->loadPiece(white);
	Piece* testPawn = new Pawn();
	testPawn->loadPiece(black);
	Piece* testRook = new Rook();
	testRook->loadPiece(black);

	testKnight->setSprite(250, 175);
	testPawn->setSprite(325, 325);
	testRook->setSprite(400, 100);

	int counter = 0;
	sf::Vector2f bPos;
	sf::Vector2f position = testKnight->getSprite().getPosition(); //Uses previous tested code
	bool whiteTurn = true; //New turn functions
	while (testWindow.isOpen()) {
		testWindow.clear();
		testWindow.draw(testBoard.getSprite()); //Draw pieces
		testWindow.draw(testKnight->getSprite()); 
		testWindow.draw(testPawn->getSprite()); //No longer checks for null, as that part is not used in this test
		testWindow.draw(testRook->getSprite());
		testWindow.display();
		Sleep(2000);

		position = testKnight->getSprite().getPosition();
		if (testPawn != nullptr) //this filters out taken pieces
		{
			bPos = testPawn->getSprite().getPosition();
		}
		if (bPos.x < position.x + 25 && bPos.x > position.x - 25 && bPos.y < position.y + 25 && bPos.y > position.y - 25)
		{
				testPawn = nullptr;//sets the opposing piece to nullptr thus removing it form the screen.
		}

		if (counter == 2) { //If on last iteration
			testWindow.close();
			cout << "Turn test: Passed!" << endl;
			return true;

		}
		else { //First 2 iterations
			if (whiteTurn) { //If it's white's turn (1st turn), move the knight
				testKnight->movePiece(sf::Vector2f(-75, 150));
				whiteTurn = false;
			}
			else if (!whiteTurn) { //If it's black's turn (2nd turn), move the rook
				whiteTurn = true; 
				testRook->movePiece(sf::Vector2f(-150, 0));
			}
			counter++;
		}
	}

	cout << "Turn test: Passed!" << endl;
	return false;

}
