#include "Piece.h"

Board::Board()
{
    loadBoard();
}

void Board::loadBoard()
{
    sf::Texture boardTexture;
    boardTexture.loadFromFile("board.png");
    setTexture(boardTexture);
}

sf::Texture Board::getTexture()
{
    return boardTexture;
}
void Board::setTexture(sf::Texture newTexture)
{
    boardTexture = newTexture;
    setSprite();
}

sf::Sprite Board::getSprite()
{
    return boardSprite;
}
void Board::setSprite()
{
    boardSprite.setTexture(boardTexture);
}