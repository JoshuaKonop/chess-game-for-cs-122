#pragma once
#include <SFML/Graphics.hpp>

class Board
{
public:
    Board();
    ~Board() {}

    void loadBoard();

    sf::Texture getTexture();
    void setTexture(sf::Texture newTexture);

    sf::Sprite getSprite();
    void setSprite();

private:
    sf::Texture boardTexture;
    sf::Sprite boardSprite;

};