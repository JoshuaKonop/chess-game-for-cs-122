#include "Piece.h"

Piece* loadPieces(Piece* newPiece, Color type, int i) //Load the pieces into their defined type
{
    if (i < 2) //If the input piece is either 0 or 1, it becomes a rook
    {
        switch (type)
        {
        case white: //Depending on if it's white or black
            newPiece = new Rook(); //Reinit as a rook 
            ((Rook*)newPiece)->loadPiece(white); //Use loadpiece function
            return newPiece;
        case black:
            newPiece = new Rook();
            ((Rook*)newPiece)->loadPiece(black);
            return newPiece;
        }
    }
    else if (i >= 2 && i < 4) //If input is 2 or 3, it becomes a knight
    {
        switch (type)
        {
        case white:
            newPiece = new Knight(); //Reinit as knight
            ((Knight*)newPiece)->loadPiece(white);
            return newPiece;
        case black:
            newPiece = new Knight();
            ((Knight*)newPiece)->loadPiece(black);
            return newPiece;
        }
    }
    else if (i >= 4 && i < 6) //If 4 or 5, it beceomes a bishop
    {
        switch (type)
        {
        case white:
            newPiece = new Bishop(); //Reinit as bishop
            ((Bishop*)newPiece)->loadPiece(white);
            return newPiece;
        case black:
            newPiece = new Bishop();
            ((Bishop*)newPiece)->loadPiece(black);
            return newPiece;
        }
    }
    else if (i == 6) //If 6, it becomes a queen
    {
        switch (type)
        {
        case white:
            newPiece = new Queen(); //Reinit as queen
            ((Queen*)newPiece)->loadPiece(white);
            return newPiece;
        case black:
            newPiece = new Queen();
            ((Queen*)newPiece)->loadPiece(black);
            return newPiece;
        }
    }
    else if (i == 7) //If 7, it becomes a king
    {
        switch (type)
        {
        case white:
            newPiece = new King(); //Reinit as king
            ((King*)newPiece)->loadPiece(white);
            return newPiece;
        case black:
            newPiece = new King();
            ((King*)newPiece)->loadPiece(black);
            return newPiece;
        }
    }
    else //Otherwise, it becomes a pawn
    {
        switch (type)
        {
        case white:
            newPiece = new Pawn(); //Reinit as pawn
            ((Pawn*)newPiece)->loadPiece(white);
            return newPiece;
        case black:
            newPiece = new Pawn();
            ((Pawn*)newPiece)->loadPiece(black);
            return newPiece;
        }
    }
}

int determinePos(int pos) //Position locking function - if the position is not centered, this function recenters it onto the board
{
    //The center of the top-left square is 25,25, and each square to the right or bottom is 75 away.
    //As such, this program determines how off each axis is, then sets it back to the center of the piece.
    if (pos <= 60) //If position is less than or equal to 60, set to 25
    {
        pos = 25;
        return pos;
    }
    else if (pos <= 135) //If position is less than or equal to 135, reset to 100
    {
        pos = 100;
        return pos;
    }
    else if (pos <= 210) //Same as the ones above
    {
        pos = 175;
        return pos;
    }
    else if (pos <= 285)
    {
        pos = 250;
        return pos;
    }
    else if (pos <= 360)
    {
        pos = 325;
        return pos;
    }
    else if (pos <= 435)
    {
        pos = 400;
        return pos;
    }
    else if (pos <= 510)
    {
        pos = 475;
        return pos;
    }
    else if (pos <= 640)
    {
        pos = 550;
        return pos;
    }
}

void printPosition(int i, sf::Vector2f position, bool taken) //Determines piece type, position, and if the piece was taken
{
    string notation = findNotation(position); //Uses findNotation function to find where on the board the piece is
    string strTaken = "";
    if (taken == true) //If the piece is taken, print x. Otherwise, don't print anything
    {
        strTaken = "x";
    }
    if (i < 2) //Depending on type of piece, print type of piece << if it's taken << where on the board it is
    {
        cout << "R" << strTaken << notation << endl; //Rook
    }
    else if (i >= 2 && i < 4)
    {
        cout << "N" << strTaken << notation << endl; //Knight
    }
    else if (i >= 4 && i < 6)
    {
        cout << "B" << strTaken << notation << endl; //Bishop
    }
    else if (i == 6)
    {
        cout << "Q" << strTaken << notation << endl; //Queen
    }
    else if (i == 7)
    {
        cout << "K" << strTaken << notation << endl; //King
    }
    else
    {
        cout << notation << endl; //Pawns just have notation
    }
}

string findNotation(sf::Vector2f position) //Function to find where the piece is on the board
{
    string xPosNotation; //x and y positions
    string yPosNotation;
    if (position.x == 25) //First determines x
    {
        xPosNotation = "a"; //First square is a
    }
    else if (position.x == 100)
    {
        xPosNotation = "b"; //Second is b
    }
    else if (position.x == 175)
    {
        xPosNotation = "c"; //Third is c, so on and so forth
    }
    else if (position.x == 250)
    {
        xPosNotation = "d";
    }
    else if (position.x == 325)
    {
        xPosNotation = "e";
    }
    else if (position.x == 400)
    {
        xPosNotation = "f";
    }
    else if (position.x == 475)
    {
        xPosNotation = "g";
    }
    else if (position.x == 550)
    {
        xPosNotation = "h";
    }

    if (position.y == 25) //Then does the same for y
    {
        yPosNotation = "8"; //First position is 8
    }
    else if (position.y == 100)
    {
        yPosNotation = "7"; //Second is 7
    }
    else if (position.y == 175)
    {
        yPosNotation = "6"; //So on and so forth
    }
    else if (position.y == 250)
    {
        yPosNotation = "5";
    }
    else if (position.y == 325)
    {
        yPosNotation = "4";
    }
    else if (position.y == 400)
    {
        yPosNotation = "3";
    }
    else if (position.y == 475)
    {
        yPosNotation = "2";
    }
    else if (position.y == 550)
    {
        yPosNotation = "1";
    }
    string notation = xPosNotation.append(yPosNotation); //Creates the final notation string, using those 2 values already acquired
    return notation;
}